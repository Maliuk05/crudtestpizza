import React,{Fragment} from 'react';
import './App.scss';

//redux
import {Provider} from 'react-redux';
import store from "./store/store";

import {BrowserRouter as Router, Route} from "react-router-dom";

import Routes from "./layout/Routes";

const  App = () => {
  return (
      <Provider store={store}>
          <Router>
              <Route component={Routes}/>
          </Router>
      </Provider>
  );
}

export default App;
