import {
    GET_PIZZAS,
    PIZZA_ERROR,
    UPDATE_PIZZA,
    REMOVE_PIZZA,
    PIZZA_ADD,
    GET_PIZZA
} from '../actions/actionTypes'

const INITIAL_STATE = {
    pizzas: [],
    pizza: null
};

export default function (state = INITIAL_STATE,action) {
    const {type,payload} = action;
    switch (type) {
        case GET_PIZZAS:
            return {
                ...state,
                pizzas: payload
            }
        case GET_PIZZA:
            return {
                ...state,
                pizza: payload
            }
        case PIZZA_ADD:
            return {
                ...state,
                pizzas: [payload, ...state.pizzas]
            }
        case REMOVE_PIZZA:
            return {
                ...state,
                pizzas: state.pizzas.filter(pizza => pizza.id !== payload)
            }
        case UPDATE_PIZZA:
            console.log('update',payload.pizza.title)
            return {
                ...state,
                pizzas: state.pizzas.map(pizza => pizza.id === payload.id ? {
                   title: payload.pizza.title,
                   price: payload.pizza.price,
                   description: payload.pizza.description,
                   linkImg: payload.pizza.linkImg
                } : pizza)
                }
        default:
            return state
    }

}