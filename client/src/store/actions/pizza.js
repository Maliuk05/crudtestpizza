import axios from 'axios';
import {
    GET_PIZZA,
    GET_PIZZAS,
    PIZZA_ADD,
    PIZZA_ERROR,
    REMOVE_PIZZA,
    UPDATE_PIZZA
} from './actionTypes';

//get pizzas
export const getPizzas = () => async dispatch => {
    try {
        const res = await axios.get('/api/pizzas')
        console.log(res);
        dispatch({type: GET_PIZZAS,payload: res.data})
    } catch (err) {
        dispatch({type: PIZZA_ERROR})
    }
}

//get pizza by id
export const getPizza = id => async dispatch => {
    try {
        const res = await axios.get(`/api/pizzas/${id}`);
        dispatch({
            type: GET_PIZZA,
            payload: res.data
        })
    } catch (err) {
        dispatch({type: PIZZA_ERROR})
    }
}

//add pizza
export const addPizza = formData => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    try {
        const res = await axios.post('/api/pizzas',formData,config);
        dispatch({type: PIZZA_ADD, payload: res.data})
    } catch (err) {
        dispatch({type: PIZZA_ERROR})
    }
}

//delete
export const deletePizza = id => async dispatch => {
    try {
        await axios.delete(`/api/pizzas/${id}`);
        dispatch({type: REMOVE_PIZZA,payload: id})
    } catch (err) {
        dispatch({
            type: PIZZA_ERROR
        })
    }
}

//update
export const updatePizza = (id, formData) => async dispatch => {
    try {
        const res = await axios.put(`/api/pizzas/${id}`,formData)
        console.log('res data',res.data)
        dispatch({type: UPDATE_PIZZA, payload:{id,pizza: res.data}})
    }catch (err) {
        console.log(`update failed id=${id}`,err)
    }
}
