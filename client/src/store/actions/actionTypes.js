export const PIZZA_ADD = 'PIZZA_ADD';
export const PIZZA_ERROR = 'PIZZA_ERROR';
export const GET_PIZZAS = 'GET_PIZZAS';
export const GET_PIZZA = 'GET_PIZZA';
export const UPDATE_PIZZA = 'UPDATE_PIZZA';
export const REMOVE_PIZZA = 'REMOVE_PIZZA';