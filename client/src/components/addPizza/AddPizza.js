import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {addPizza} from "../../store/actions/pizza";
import './AddPizza.scss';

const AddPizza = ({onClose}) => {

    const dispatch = useDispatch();
    const [formData, setFormData] = useState({
        title: '',
        price: '',
        description: '',
        linkUrl: ''
    });

    const onSubmit = e => {
        e.preventDefault();
        dispatch(addPizza(formData));
        setFormData({
            title: '',
            price: '',
            description: '',
            linkImg: ''
            })
    }

    const onChange = e=> setFormData({...formData,[e.target.name]: e.target.value});
    const {title,price,description,linkImg} = formData;
    return (
     <div>
         <h3 className={'title'}>Add new pizza</h3>
         <form onSubmit={e=>onSubmit(e)} className='form-container'>
                 <label>Name a Pizza</label>
                 <input type="text" name="title" value={title} onChange={e=>onChange(e)}/>
                 <label>Price</label>
                 <input type="text" name="price" value={price} onChange={e=>onChange(e)}/>
                 <label>Description</label>
                 <input type="text" name="description" value={description} onChange={e=>onChange(e)}/>
                 <label>URL image</label>
                 <input type="text" name="linkImg" value={linkImg} onChange={e=>onChange(e)}/>
                 <div className='btn-group'>
                     <button type='submit' className='btnSubmit'>Add</button>
                     <button onClick={()=>onClose()} className='btnSubmit'>No thanks</button>
                 </div>

         </form>
     </div>
    )
}

export default AddPizza