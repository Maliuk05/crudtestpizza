import React, {useEffect, useState} from 'react';
import {useParams,Redirect,Link} from 'react-router-dom';

import {useDispatch,useSelector} from 'react-redux';
import {getPizza, updatePizza,deletePizza} from "../../store/actions/pizza";

import './PizzaEdit.scss';

const Pizza = () => {
    const [formData, setFormData] = useState({
        title: '',
        price: '',
        description: '',
        linkImg:''
    });
    const [redirect, setRedirect] = useState(false);

    const {id} = useParams();
    console.log(id)
    const pizza = useSelector(state => state.pizza.pizza)
    // console.log(pizza)
    const dispatch = useDispatch();

    const onRemove = () => {
        dispatch(deletePizza(id))
        setRedirect(true)
    }

    useEffect( () => {
        const fetchData = async () => {
            await dispatch(getPizza(id))
        }
        fetchData();
    }, [id,dispatch]);
    const onChange = e=> setFormData({...formData,[e.target.name]: e.target.value});

    const onSubmit = e => {
        e.preventDefault();
        dispatch(updatePizza(id,formData))
        setRedirect(true)
    };

    if (redirect) {
        return <Redirect to={'/'}/>
    }

    const {title,description,linkImg,price} = formData;
    return (
        <div>
            <Link to={'/'}><button className={'link-btn'}>To main page</button></Link>
            {pizza !== null ? (
                <div className='edit-container'>
                    <img src={pizza.linkImg} alt=""/>
                    <form className='form-edit' onSubmit={e=>onSubmit(e)}>
                        <div className='input-group'>
                            <div className="input-item">
                                <label>Url</label>
                                <input type="text"
                                       placeholder={'url'}
                                       name={'linkImg'}
                                       value={linkImg}
                                       onChange={e=>onChange(e)}
                                />
                            </div>
                            <div className="input-item">
                                <label>title</label>
                                <input type="text"
                                       placeholder={'name pizza'}
                                       name={'title'}
                                       onChange={e=>onChange(e)}
                                       value={title}
                                />
                            </div>
                        </div>
                        <div className='input-group'>
                            <div className="input-item">
                                <label>Price</label>
                                <input type="text"
                                       placeholder={'price pizza'}
                                       name={'price'}
                                       value={price}
                                       onChange={e=>onChange(e)}
                                />
                            </div>
                          <div className="input-item">
                              <label>description</label>
                              <input type="text"
                                     placeholder={'description pizza'}
                                     name={'description'}
                                     value={description}
                                     onChange={e=>onChange(e)}
                              />
                          </div>
                        </div>
                        <div className='btn-group'>
                            <button type='submit' className='updateBtn'>Update</button>
                            <button className='removeBtn' onClick={()=>onRemove()}>Delete</button>
                        </div>

                    </form>

                </div>


            ) : null

            }
            {/*{pizza.title}*/}
        </div>
    )
}

export default Pizza