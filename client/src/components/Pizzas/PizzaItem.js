import React from 'react';
import './PizzaItem.scss';
import {Link} from "react-router-dom";

const PizzaItem = ({data:{title,price,description,linkImg},id}) => {
    return (
        <div className='card'>
            <img src={linkImg} alt={'eatImg'}/>
            <div className='card-description'>
                <h1 className='card-description-title'>{title}</h1>
                <h3 className='card-description-price'>{price} $</h3>
                <span className='card-description-specification'>{description}</span>
            </div>
            <Link to={`/edit/${id}`}>
                <button className={'btn'}>Edit</button>
            </Link>

        </div>
    )
}

export default PizzaItem;