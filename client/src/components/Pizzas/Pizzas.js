import React, {Fragment, useEffect, useState} from 'react';

//redux
import {useDispatch,useSelector} from "react-redux";
import {getPizzas} from "../../store/actions/pizza";

import PizzaItem from "./PizzaItem";
import './Pizza.scss';

const Pizzas = () => {
    const dispatch = useDispatch();
    const pizzas = useSelector(state => state.pizza.pizzas)
    console.log('pizzas',pizzas)

    useEffect(() => {
        dispatch(getPizzas());
    }, [dispatch]);

    return (
        <>
        <div className='container'>
            {pizzas.map((pizza,index) => (
                <PizzaItem data={pizza} key={index} id={pizza.id}/>
            ))}
        </div>
            </>
    )
}
export default Pizzas;