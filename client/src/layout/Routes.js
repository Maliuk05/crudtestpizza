import React, {useState} from "react";
import {Switch, Route} from 'react-router-dom';
import Pizzas from "../components/Pizzas/Pizzas";
import Pizza from "../components/Pizza/Pizza";
import Popup from "reactjs-popup";
import AddPizza from "../components/addPizza/AddPizza";
const Routes = () => {
    const [show, setShow] = useState(false);

    const onOpen = () => {
        setShow(!show)
    }
    const onClose = () => {
        setShow(false);
    }
    return (

        <>
           <header style={{
               height:' 60px',
               backgroundColor: '#e9ecec',
               display: 'flex',
               alignItems: 'center',
               justifyContent: 'flex-end'
           }}>
               <button
                   style={{
                       backgroundColor: '#5a5e5e',
                       color: '#fff',
                       padding: '10px 20px',
                       marginRight: '40px'
                   }}
                   onClick={()=>onOpen()}>
                   Add Pizza
               </button>
           </header>
            <Popup open={show}
                   position="center center"
                   modal={true}
                   children={<AddPizza onClose={onClose}/>}
            />
            <Switch>
                <Route exact path='/' component={Pizzas}/>
                <Route exact path='/edit/:id' component={Pizza}/>
            </Switch>
        </>
    )
}

export default Routes;