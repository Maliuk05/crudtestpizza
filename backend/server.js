const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const corsOptions = {
    origin:"http://localhost:5000"
}
//cors
app.use(cors(corsOptions));

//parse application-json
app.use(bodyParser.json());

//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}))

const db = require("./models");
db.sequelize.sync();
db.sequelize.sync({ force: true }).then(() => {
    console.log("Drop and re-sync db.");
});

app.get('/',(req,res) => res.json({message: 'SERVER IS RUNNING'}))


const PORT = process.env.PORT || 5000;

require('./routes/pizza.routes')(app)

app.listen(PORT, () => console.log(`Server started on port ${PORT}`))