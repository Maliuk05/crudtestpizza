module.exports = (sequalize,Sequelize) => {
    const Pizza = sequalize.define('pizza', {
        title: {
            type:Sequelize.STRING
        },
        price: {
          type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        linkImg: {
            type: Sequelize.STRING
        }
    });
    return Pizza;
}