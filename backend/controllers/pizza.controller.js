const db = require('../models');
const Pizza = db.pizzas;
const Op = db.Sequelize.Op;

//@create and save new Pizza
exports.create = (req,res) => {
//validate
    if (!req.body.title) {
        res.status(400).send({
            message: "Content can not be empty"
        })
        return
    }

    const pizza = {
        title: req.body.title,
        price: req.body.price,
        description: req.body.description,
        linkImg: req.body.linkImg
    }
    Pizza.create(pizza)
        .then(data=>res.send(data))
        .catch(err=> {
            res.status(500).send({
                message:err.message || 'create error'
            })
        })
};

//@all pizzas
exports.findAll = (req,res) => {
    const title = req.query.title;
    let condition = title ? {title: {[Op.iLike]:`%${title}%`}} : null

    Pizza.findAll({where:condition})
        .then(data=>res.send(data))
        .catch(err => {
            res.status(500).send({
                message: err.message || 'get all error'
            })
        })
};

//@find a single pizza
exports.findOne = (req,res) => {
    const id = req.params.id;

    Pizza.findByPk(id)
        .then(data=> res.send(data))
        .catch(err => {
            res.status(500).send({
                message: err.message || 'get pizza by id error'
            })
        })
}

//@update pizza by id
exports.update = (req, res) => {
    console.log('reqbody',req.body)

    const id = req.params.id;
    const payload = req.body
    Pizza.update(req.body, {
        where: { id: id }
    })
        .then((num,data) => {
            if (num == 1) {
                console.log('payload',payload)
                res.send(payload);
            } else {
                res.send({
                    message: `Cannot update Pizza with id=${id}. Maybe Pizza was not found or req.body is empty!`
                });
            }
        })
        .then(data=> res.send(data))
        .catch(err => {
            res.status(500).send({
                message: "Error updating Pizza with id=" + id
            });
        });
};
//@delete pizza by id
exports.delete = (req,res) => {
    const id = req.params.id;

    Pizza.destroy({
        where: {id: id}
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: 'Pizza was deleted!'
                })
            } else {
                res.send({
                    message: `cannot delete pizza with id=${id}`
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message:'could not delete with id=' + id
            })
        })
}