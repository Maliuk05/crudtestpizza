module.exports = {
    HOST: 'localhost',
    USER: 'maliuk',
    PASSWORD:'123456',
    DB: 'pizzas_db',
    dialect: "postgres",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
}