module.exports = app => {
    const pizzas = require('../controllers/pizza.controller');

    let router = require('express').Router();

    //@route POST api/pizzas
    //@desc create a pizza
    //@access Public
    router.post('/', pizzas.create);

    //@route GET api/pizzas
    //@desc get all pizzas
    //@access Public
    router.get('/', pizzas.findAll);

    //@route GET api/pizzas/:id
    //@desc get pizza by id
    //@access Public
    router.get('/:id', pizzas.findOne);

    //@route PUT api/pizzas/:id
    //@desc put pizza by id
    //@access Public
    router.put('/:id', pizzas.update);

    //@route delete api/pizzas/:id
    //@desc put pizza by id
    //@access Public
    router.delete('/:id', pizzas.delete);

    app.use('/api/pizzas', router);
}